import java.util.Random;

public class RandomNumGenerator {
    private Random random;

    public RandomNumGenerator(Random random) {
        this.random = random;
    }

    public int getRandomNumber() throws MaxException {

        int num = random.nextInt(10);

        if(num <= 5) {
            throw new MaxException("Сгенерированное число - " + num);
        }

        return num;
    }
}
