import org.apache.log4j.Logger;

import java.util.Random;

public class Main {
    private static final Logger LOG_CONSOLE = Logger.getLogger("loggerConsole");
    private static final Logger LOG_FILE = Logger.getLogger("loggerFile");

    public static void main(String[] args) {
        Random random = new Random();
        RandomNumGenerator randomNumGenerator = new RandomNumGenerator(random);

        try {
            int getRandomNumber = randomNumGenerator.getRandomNumber();
            LOG_CONSOLE.info("Приложение успешно запущено");
            LOG_FILE.info("Приложение успешно запущено");
        } catch (MaxException e) {
            LOG_CONSOLE.error(e.getMessage());
            LOG_FILE.error(e.getMessage());
        }

    }

}
